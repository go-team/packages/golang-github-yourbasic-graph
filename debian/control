Source: golang-github-yourbasic-graph
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any
Standards-Version: 4.5.0
Homepage: https://github.com/yourbasic/graph
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-yourbasic-graph
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-yourbasic-graph.git
XS-Go-Import-Path: github.com/yourbasic/graph
Testsuite: autopkgtest-pkg-go

Package: golang-github-yourbasic-graph-dev
Architecture: all
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: graph algorithms and data structures
 This package contains a Golang library of basic graph algorithms
 and topological ordering.
 .
 This library offers efficient and well-tested algorithms for
  - breadth-first and depth-first search,
  - topological ordering,
  - strongly and weakly connected components,
  - bipartion,
  - shortest paths,
  - maximum flow,
  - Euler walks,
  - and minimum spanning trees.
 The algorithms can be applied to any graph data structure implementing
 the two Iterator methods: Order, which returns the number of vertices,
 and Visit, which iterates over the neighbors of a vertex.
